import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:ping/data/model/authentication/register_user.dart';
import 'package:ping/data/model/auth_user.dart';
import 'package:ping/data/repository/authentication/authentication_repository_impl.dart';
import 'package:ping/generated/locale_key.g.dart';
import 'package:ping/screens/home_layout.dart';
import 'package:ping/utils/cache_helper.dart';
import '../../locator.dart';
import 'package:easy_localization/easy_localization.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());

  String? verificationId;
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  void register(
      {required RegisterUser registerUser, required bool termsConditions}) {
    emit(RegisterLoading());

    if (!termsConditions) {
      emit(
        RegisterError(
          message: LocaleKeys.terms_and_conditions_must_be_agreed.tr(),
        ),
      );
    } else {
      locator
          .get<AuthenticationRepositoryImpl>()
          .register(registerUser: registerUser)
          .then((user) {
        CacheHelper.saveData(key: 'isLoggedIn', value: true);
        emit(RegisterCompleted(user));
      }).onError((error, stackTrace) {
        emit(RegisterError(message: error.toString()));
      });
    }
  }

  void sendOtp(
      {required String phoneNumber, required BuildContext context}) async {
    emit(OtpCodeSending());
    print(state);
    phoneNumber = '+218$phoneNumber';
    await firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        codeSent: (String verificationId, int? forceResendingToken) async {
          print('code sent');
          this.verificationId = verificationId;
          Navigator.pushNamed(context, '/otp');

          emit(OtpCodeSent());
        },
        verificationFailed: (FirebaseAuthException error) async {
          emit(OtpError(error.message!));
        },
        codeAutoRetrievalTimeout: (String verificationId) {},
        verificationCompleted: (PhoneAuthCredential phoneAuthCredential) async {
          emit(OtpVerifying());

          Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomeLayout()));

          await firebaseAuth.signInWithCredential(phoneAuthCredential);
          emit(OtpVerified());
        });
  }

  void signInWithCredential(
      {required String otp, required BuildContext context}) async {
    emit(OtpVerifying());
    AuthCredential authCredential = PhoneAuthProvider.credential(
        verificationId: verificationId!, smsCode: otp);

    await firebaseAuth
        .signInWithCredential(authCredential)
        .then((value) => {
              emit(OtpVerified()),
              Navigator.pop(context),
            })
        .onError(
          (error, stackTrace) => {
            print(error),
            emit(
              OtpError(
                error.toString(),
              ),
            ),
          },
        );
  }
}
