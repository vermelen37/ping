part of 'register_cubit.dart';

@immutable
abstract class RegisterState extends Equatable {}

class RegisterInitial extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class RegisterLoading extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class RegisterCompleted extends RegisterState {
  late final AuthUser user;

  RegisterCompleted(this.user);

  @override
  // TODO: implement props
  List<Object?> get props => [user];
}

class RegisterError extends RegisterState {
  final String message;

  RegisterError({required this.message});

  @override
  // TODO: implement props
  List<Object?> get props => [message];
}

class OtpCodeSending extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpCodeSent extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpError extends RegisterState {
  final String message;

  OtpError(this.message);

  @override
  // TODO: implement props
  List<Object?> get props => [message];
}

class OtpVerifying extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpVerified extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpVerifiedError extends RegisterState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}
