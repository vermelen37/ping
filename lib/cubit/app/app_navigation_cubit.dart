import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:ping/screens/dashboard/dashboard_page.dart';
import 'package:ping/screens/profile_page.dart';

part 'app_navigation_state.dart';

class AppNavigationCubit extends Cubit<AppNavigationState> {
  AppNavigationCubit() : super(AppNavigationInitial());

  int currentIndex = 0;
  List<Widget> bottomScreens = [
    DashboardPage(),
    DashboardPage(),
    ProfilePage(),
  ];

  void changeBottomPage(int index) {
    currentIndex = index;
    emit(AppNavigationChangeBottomPage());
  }
}
