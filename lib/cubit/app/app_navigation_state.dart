part of 'app_navigation_cubit.dart';

@immutable
abstract class AppNavigationState {}

class AppNavigationInitial extends AppNavigationState {}
class AppNavigationChangeBottomPage extends AppNavigationState {}
