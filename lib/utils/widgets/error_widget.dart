import 'package:flutter/material.dart';
import 'package:ping/utils/theme/themes.dart';

class ValidationError extends StatelessWidget {
  const ValidationError({
    Key? key,
    required this.screenSize,
    required this.error,
  }) : super(key: key);

  final Size screenSize;
  final String error;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        width: screenSize.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(4),
          ),
          color: AppTheme.errorColor,
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 8.0, left: 8.0),
          child: Text(
            error,
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
