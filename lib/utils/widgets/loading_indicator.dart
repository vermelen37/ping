import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ping/utils/theme/themes.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      color: AppTheme.primaryColor,
      size: 35.0,
    );
  }
}
