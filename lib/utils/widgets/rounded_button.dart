import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;
  final Color color;
  final double radius;
  final double? height;
  final double? width;
  final Color? titleColor;
  final Color? borderColor;

  const RoundedButton({
    required this.onPressed,
    required this.title,
    required this.color,
    required this.radius,
    this.height = 35,
    this.width = 75,
    this.titleColor = Colors.white,
    this.borderColor = Colors.white,
  }) : super();

  @override
  Widget build(BuildContext context) {
    double fontFactor = MediaQuery.of(context).textScaleFactor;
    return ButtonTheme(
      height: height!,
      minWidth: width!,
      child: MaterialButton(
        onPressed: onPressed,
        color: color,
        textColor: Colors.white,
        child: Text(
          title,
          style: TextStyle(fontSize: fontFactor * 16, color: titleColor),
        ),
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: borderColor!, width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(radius),
        ),
      ),
    );
  }
}
