class Helper {

  

  String getValidationError({required String string}) {
    var re = RegExp(r'\[(.*?)\]');
    var match = re.firstMatch(string);
    var s = match!.group(0);
    s = s!.replaceAll('[', '');
    s = s.replaceAll(']', '');
    return s;
  }
}
