import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiBaseHelper {
  String apiUrl = '';
  Dio _dio = Dio();

  ApiBaseHelper() {
    if (kReleaseMode) {
      _dio.options.baseUrl = GlobalConfiguration().get('live_url');
    } else {
      _dio.options.baseUrl = GlobalConfiguration().get('local_url');
    }
  }

  Future<dynamic> postDio(
    String path,
    body,
  ) async {
    var token = await getToken();
    _dio.options.headers["Authorization"] = "Bearer $token";
    try {
      var response = await _dio.post(
        '/$path',
        data: body,
      );
      return response.data;
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } on DioError catch (e) {
      throw e.response!.data;
    }
  }

  Future<dynamic> puttDio(
    String path,
    body,
  ) async {
    var token = await getToken();
    _dio.options.headers["Authorization"] = "Bearer $token";
    try {
      var response = await _dio.put(
        '$apiUrl/$path',
        data: body,
      );
      return response.data;
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } on DioError catch (e) {
      throw e.response!.data;
    }
  }

  Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final token = prefs.getString(key) ?? 'null';
    return token;
  }
}
