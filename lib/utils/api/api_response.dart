class ApiResponse<T> {
  APIStatus? status;
  T? data;
  String? message;

  ApiResponse.onInit() : status = APIStatus.onInit;

  ApiResponse.onLoading() : status = APIStatus.onLoading;

  ApiResponse.onSuccess({required this.data}) : status = APIStatus.onSuccess;

  ApiResponse.onFailure({required this.message}) : status = APIStatus.onFailure;

  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $data";
  }

  ApiResponse();
}

enum APIStatus { onInit, onLoading, onSuccess, onFailure }
