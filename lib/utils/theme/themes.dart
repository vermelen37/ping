import 'package:flutter/material.dart';

var lightThemeData = new ThemeData(
  brightness: Brightness.light,
  primarySwatch: createMaterialColor(Color(0xFF137bfe)),
  toggleableActiveColor: createMaterialColor(Color(0xFF137bfe)),
  errorColor: AppTheme.errorColor,
  buttonColor: AppTheme.secondaryColor,
  backgroundColor: const Color(0xFFE5E5E5),
  fontFamily: 'tajawal',
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    foregroundColor: createMaterialColor(Color(0xFF137bfe)),
  ),

// primarySwatch: Colors.grey,
  // primaryColor: Colors.white,
  // brightness: Brightness.light,
  // accentColor: Colors.black,
  // accentIconTheme: IconThemeData(color: Colors.white),
  // dividerColor: Colors.white54,
  // indicatorColor: AppTheme.primaryColor,
);

var darkThemeData = ThemeData(
  brightness: Brightness.dark,
  primarySwatch: createMaterialColor(Color(0xFF137bfe)),
  toggleableActiveColor: createMaterialColor(Color(0xFF137bfe)),
  errorColor: AppTheme.errorColor,
  buttonColor: AppTheme.secondaryColor,
  backgroundColor: const Color(0xFFE5E5E5),
  fontFamily: 'tajawal',
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    foregroundColor: createMaterialColor(Color(0xFF137bfe)),
  ),

  // primarySwatch: Colors.grey,
  // primaryColor: AppTheme.primaryColor,
  // brightness: Brightness.dark,
  // accentColor: Colors.white,
  // accentIconTheme: IconThemeData(color: Colors.black),
  // dividerColor: Colors.black12,
  // hintColor: Colors.white,
  // indicatorColor: AppTheme.primaryColor,
);

class AppTheme {
  static const Color notWhite = Color(0xFFEDF0F2);
  static const Color nearlyWhite = Color(0xFFFFFFFF);
  static const Color chipBackground = Color(0xFFEEF1F3);
  static const Color spacer = Color(0xFFF2F2F2);
  static const Color grey = Color(0xFF3A5160);
  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color nearlyBlack = Color(0xFF213333);
  static Color nearlyGreen = HexColor("6fbd87");
  static Color nearlyGrey = HexColor("f2f6ff");
  static Color primaryColor = Color(0xFF137bfe);

  // static Color primaryColor = HexColor("d81159");
  static Color whitePrimaryColor = HexColor("4138BC");
  static Color secondaryColor = Color(0xFF272727);
  static Color transparent = HexColor('00FFFFFF');

  static Color errorColor = HexColor("F1095E");
  static Color warningColor = Colors.amber;
  static Color goldColor = HexColor('fdee31');
  static Color successColor = HexColor('33C707');

  //buttons color
  static Color whiteBrownColor = HexColor('ECCB94');
  static Color whitePurpleColor = HexColor('D7D6FE');
  static Color shadowColor = HexColor('D7D6FE');
  static Color whiteBlueColor = HexColor('94C6EC');
  static Color cartBackground = HexColor('#F6F8FF');

  //Appbar Colors
  static Color authAppbarColor = Color(0XFFF7F9FA);
  static Color authAppbarContentColor = Color(0XFF707070);

  //Text background Colors
  static Color textBackground = Color(0XFFDDE4F6).withOpacity(.5);
  static Color textAndIconColor = Color(0XFF707070);
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}


MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map<int, Color> swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}