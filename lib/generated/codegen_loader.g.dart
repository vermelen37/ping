// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "login": "Login",
  "log_out": "Logout",
  "welcome_to": "Welcome to",
  "ping": "Ping",
  "next": "Next",
  "we_provide_best_offers_of_i_s_p": "We provide you with the best offers of ISP",
  "register": "Create your account",
  "create_your_account_and_choose_its_type_seller_or_customer": "Create your account, and choose its type (seller or customer)",
  "get_started": "Get Started",
  "sign_up": "Sign up",
  "phone": "Phone",
  "phone_number": "Phone number",
  "password": "Password",
  "the_username_field_is_requires": "The Username field is requires",
  "the_email_field_is_requires": "The Email field is requires",
  "the_password_field_is_requires": "The Password field is requires",
  "the_confirm_password_field_is_requires": "The confirm Password field is requires",
  "passwords_does_not_match": "Passwords doesn't match",
  "please_enter_your_credentials": "Please enter your credentials",
  "the_password_is_incorrect": "The password is incorrect",
  "search": "Search",
  "previous": "Previous",
  "skip": "Skip",
  "settings": "Settings",
  "required_field": "Required field",
  "this_field_requires_a_valid_email": "This field requires a valid email address",
  "email": "Email",
  "username": "Username",
  "password_length_is_more_than_5": "Password length is more than 5 character",
  "do_not_have_an_account": "Don't have an account?",
  "terms_conditions": "Terms conditions",
  "agree_with": "Agree with?",
  "do_have_an_account": "Do have an account?",
  "forget_password": "Forget Password?",
  "confirm_password": "Confirm password",
  "terms_and_conditions_must_be_agreed": "Terms and conditions must be agreed",
  "confirm_password_does_not_match": "Confirm password does not match",
  "the_email_has_already_been_taken": "The email has already been taken",
  "the_phone_has_already_been_taken": "The phone has already been taken",
  "something_went_wrong_please_try_again_later": "Something went wrong, please try again later",
  "invalid_credentials": "Invalid Credentials",
  "send_message": "Send Message ...",
  "user_profile": "User Profile",
  "there_are_no_conversations": "There are no conversations",
  "create_new_account": "Create new account"
};
static const Map<String,dynamic> ar = {
  "login": "تسجيل الدخول",
  "welcome_to": "مرحباً بك في",
  "log_out": "تسجيل الخروج",
  "register": "إنشئ حسابك",
  "ping": "بنق",
  "we_provide_best_offers_of_i_s_p": "نوفر لك أفضل عروض مزودي خدمات الانترنت",
  "create_your_account_and_choose_its_type_seller_or_customer": "أنشئ حسابك واختر نوعه (بائع أو عميل)",
  "next": "التالي",
  "get_started": "إنطلق",
  "sign_up": "تسجيل",
  "phone": "رقم الهاتف",
  "phone_number": "رقم الهاتف",
  "this_field_is_required": "هذا الحقل إجباري",
  "the_username_field_is_requires": "اسم المستخدم حقل إجباري",
  "the_email_field_is_requires": "البريد الإلكتروني حقل إجباري",
  "the_password_field_is_requires": "كلمة المرور حقل إجباري",
  "the_confirm_password_field_is_requires": "تأكيد كلمة المرور حقل إجباري",
  "passwords_does_not_match ": "كلمات المرور غير متطابقة",
  "password": "كلمة المرور",
  "please_enter_your_credentials": "الرجاء إدخال معلومات المرور",
  "the_password_is_incorrect": "كلمة المرور غير صحيحة",
  "search": "بحث",
  "previous": "السابق",
  "skip": "تخطي",
  "settings": "الإعدادات",
  "required_field": "حقل إجباري",
  "this_field_requires_a_valid_email": "صيغة البريد الإلكتروني غير صحيحة",
  "email": "البريد الإلكتروني",
  "username": "اسم المستخدم",
  "password_length_is_more_than_5": "كلمة المرور أكثر من 5",
  "do_not_have_an_account": "لا تملك حساباً",
  "terms_conditions": "الشروط والأحكام",
  "do_have_an_account": "هل تملك حساباً بالفعل؟",
  "agree_with": "موافق على؟",
  "forget_password": "نسيت كلمة المرور؟",
  "confirm_password": "تأكيد كلمة المرور",
  "terms_and_conditions_must_be_agreed": "يجب الموافقة على الشروط والأحكام",
  "confirm_password_does_not_match": "تأكيد كلمة المرور غير مطابق",
  "the_email_has_already_been_taken": "البريد الإلكتروني مستخدم مسبقاً",
  "the_phone_has_already_been_taken": "رقم الهاتف مستخدم مسبقاً",
  "something_went_wrong_please_try_again_later": "حدث خطأ ما، يرجى المحاولة لاحقاً",
  "invalid_credentials": "بيانات الدخول غير صحيحة",
  "user_profile": "الملف الشخصي",
  "there_are_no_conversations": "لا توجد محادثات",
  "create_new_account": "تسجيل حساب جديد"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "ar": ar};
}
