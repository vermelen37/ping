// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const login = 'login';
  static const log_out = 'log_out';
  static const welcome_to = 'welcome_to';
  static const ping = 'ping';
  static const next = 'next';
  static const we_provide_best_offers_of_i_s_p = 'we_provide_best_offers_of_i_s_p';
  static const register = 'register';
  static const create_your_account_and_choose_its_type_seller_or_customer = 'create_your_account_and_choose_its_type_seller_or_customer';
  static const get_started = 'get_started';
  static const sign_up = 'sign_up';
  static const phone = 'phone';
  static const phone_number = 'phone_number';
  static const password = 'password';
  static const the_username_field_is_requires = 'the_username_field_is_requires';
  static const the_email_field_is_requires = 'the_email_field_is_requires';
  static const the_phone_field_is_requires = 'the_phone_field_is_requires';
  static const the_password_field_is_requires = 'the_password_field_is_requires';
  static const please_enter_valid_phone_number = 'please_enter_valid_phone_number';
  static const the_confirm_password_field_is_requires = 'the_confirm_password_field_is_requires';
  static const the_password_is_more_than_8_characters = 'the_password_is_more_than_8_characters';
  static const passwords_does_not_match = 'passwords_does_not_match';
  static const please_enter_your_credentials = 'please_enter_your_credentials';
  static const the_password_is_incorrect = 'the_password_is_incorrect';
  static const search = 'search';
  static const previous = 'previous';
  static const skip = 'skip';
  static const settings = 'settings';
  static const required_field = 'required_field';
  static const this_field_requires_a_valid_email = 'this_field_requires_a_valid_email';
  static const email = 'email';
  static const username = 'username';
  static const password_length_is_more_than_5 = 'password_length_is_more_than_5';
  static const do_not_have_an_account = 'do_not_have_an_account';
  static const terms_conditions = 'terms_conditions';
  static const agree_with = 'agree_with';
  static const do_have_an_account = 'do_have_an_account';
  static const forget_password = 'forget_password';
  static const confirm_password = 'confirm_password';
  static const terms_and_conditions_must_be_agreed = 'terms_and_conditions_must_be_agreed';
  static const confirm_password_does_not_match = 'confirm_password_does_not_match';
  static const the_email_has_already_been_taken = 'the_email_has_already_been_taken';
  static const the_phone_has_already_been_taken = 'the_phone_has_already_been_taken';
  static const something_went_wrong_please_try_again_later = 'something_went_wrong_please_try_again_later';
  static const invalid_credentials = 'invalid_credentials';
  static const send_message = 'send_message';
  static const user_profile = 'user_profile';
  static const there_are_no_conversations = 'there_are_no_conversations';
  static const create_new_account = 'create_new_account';
  static const failed_to_send_verification_code_try_again_later = 'failed_to_send_verification_code_try_again_later';

}
