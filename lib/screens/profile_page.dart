import 'package:flutter/material.dart';
import 'package:ping/utils/cache_helper.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          InkWell(
            onTap: () {
              CacheHelper.removeData(key: 'isLoggedIn').then(
                (value) => {
                  if (value)
                    {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/register', (route) => false)
                    }
                },
              );
            },
            child: Text(
              'Logout',
            ),
          ),
        ],
      ),
    );
  }
}
