import 'package:flutter/material.dart';
import 'package:flutter_advanced_avatar/flutter_advanced_avatar.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:ping/utils/theme/themes.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _searchBox(),
        _onlineUsers(),
      ],
    );
  }

  _searchBox() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderTextField(
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.symmetric(vertical: 7.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: AppTheme.primaryColor,
              radius: 3,
              child: Icon(
                PhosphorIcons.magnifying_glass_bold,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
          hintText: 'hint',
        ),
        name: 'email',
      ),
    );
  }

  Row _onlineUsers() {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 100,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 20,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AdvancedAvatar(
                    size: 60,
                    statusSize: 14,
                    statusAngle: 50,
                    statusColor: Colors.green,
                    image: NetworkImage(
                        'https://www.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg'),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
