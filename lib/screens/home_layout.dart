import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:ping/cubit/app/app_navigation_cubit.dart';
import 'package:ping/locator.dart';
import 'package:ping/utils/theme/themes.dart';

class HomeLayout extends StatefulWidget {
  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  @override
  Widget build(BuildContext context) {
    AppNavigationCubit navigationCubit = locator.get<AppNavigationCubit>();

    return BlocConsumer<AppNavigationCubit, AppNavigationState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              title: Text(
                "Ping",
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Colors.black, fontFamily: 'tajawal-bold'),
              ),
              actions: [
                IconButton(
                    icon: Icon(
                      PhosphorIcons.chat_circle_bold,
                      color: Colors.black,
                    ),
                    onPressed: () {}),
                IconButton(
                    icon: Icon(
                      PhosphorIcons.bell_bold,
                      color: Colors.black,
                    ),
                    onPressed: () {})
              ],
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(PhosphorIcons.house_bold),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(PhosphorIcons.buildings_bold),
                  label: 'Business',
                ),
                BottomNavigationBarItem(
                  icon: Icon(PhosphorIcons.user_bold),
                  label: 'School',
                ),
              ],
              currentIndex: navigationCubit.currentIndex,
              selectedItemColor: AppTheme.primaryColor,
              onTap: (index) {
                navigationCubit.changeBottomPage(index);
              },
            ),
            body: navigationCubit.bottomScreens[navigationCubit.currentIndex],
          );
        },
        listener: (context, state) {});
  }
}
