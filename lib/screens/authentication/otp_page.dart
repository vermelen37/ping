import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:ping/cubit/authentication/register_cubit.dart';
import 'package:ping/utils/theme/themes.dart';
import 'package:ping/utils/widgets/error_widget.dart';
import 'package:ping/utils/widgets/loading_indicator.dart';
import 'package:ping/utils/widgets/rounded_button.dart';
import 'package:ping/locator.dart';

class OtpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final otpController = TextEditingController();
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: PinCodeTextField(
              controller: otpController,
              appContext: context,
              pastedTextStyle: TextStyle(
                color: Colors.green.shade600,
                fontWeight: FontWeight.bold,
              ),
              length: 6,
              obscureText: false,
              obscuringCharacter: '*',
              animationType: AnimationType.fade,
              validator: (v) {
                if (v!.length < 3) {
                  return null;
                } else {
                  return null;
                }
              },
              pinTheme: PinTheme(
                  shape: PinCodeFieldShape.circle,
                  borderWidth: 1,
                  inactiveColor: Colors.blue.withOpacity(0.4),
                  fieldHeight: 50,
                  fieldWidth: 50,
                  activeColor: Colors.grey.withOpacity(0.4),
                  activeFillColor: Colors.black12,
                  inactiveFillColor: Colors.white,
                  selectedFillColor: Colors.white),
              cursorColor: AppTheme.primaryColor,
              animationDuration: Duration(milliseconds: 300),
              textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
              backgroundColor: Colors.white.withOpacity(0.0),
              enableActiveFill: true,
              keyboardType: TextInputType.number,
              onCompleted: (v) {
                // smsSent = v;
              },
              onTap: () {},
              onChanged: (value) {},
              beforeTextPaste: (text) {
                return true;
              },
            ),
          ),
          BlocConsumer<RegisterCubit, RegisterState>(
              listener: (context, state) {},
              builder: (context, state) {
                print('OTP');
                print(state.toString());
                print(state.toString());
                if (state is OtpVerifying) {
                  return LoadingIndicator();
                } else if (state is OtpCodeSent) {
                  return Container();
                } if (state is RegisterError) {
                  print('RegisterError');
                  return ValidationError(screenSize: screenSize, error: state.message);
                }if (state is OtpError) {
                  return ValidationError(screenSize: screenSize, error: state.message);
                }
                return Column();
              }),
          RoundedButton(
            onPressed: () {
              locator.get<RegisterCubit>().signInWithCredential(
                  otp: otpController.text, context: context);
            },
            title: 'Verify',
            color: AppTheme.primaryColor,
            radius: 45.0,
            borderColor: AppTheme.primaryColor,
            width: 140,
            height: 42,
          )
        ],
      ),
    );
  }
}
