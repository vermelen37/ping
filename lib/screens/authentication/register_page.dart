import 'dart:convert';
import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ping/cubit/authentication/register_cubit.dart';
import 'package:ping/data/model/authentication/register_user.dart';
import 'package:ping/generated/locale_key.g.dart';
import 'package:ping/utils/helper.dart';
import 'package:ping/utils/theme/themes.dart';
import 'package:ping/utils/widgets/error_widget.dart';
import 'package:ping/utils/widgets/loading_indicator.dart';
import 'package:ping/utils/widgets/ping_avatar.dart';
import 'package:ping/utils/widgets/rounded_button.dart';
import 'package:roundcheckbox/roundcheckbox.dart';
import '../../locator.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

enum RegisterUiState { RegisterUI, OtpUI }

class _RegisterPageState extends State<RegisterPage> {
  final _picker = ImagePicker();
  final _formKey = GlobalKey<FormBuilderState>();
  bool _termsConditions = false;
  File? _image;

  Future getImage() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(
              top: screenSize.height * 0.10, right: 30, left: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FormBuilder(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _titleWidget(context),
                    _profileAvatarWidget(),
                    _fullName(context: context),
                    _fullPhoneNumber(context: context),
                    _fullPassword(context: context),
                    _fullConfirmPassword(context: context),
                    _termsConditionsWidget(context: context),
                  ],
                ),
              ),
              _registerState(screenSize),
              _submitButton(screenSize: screenSize),
              _loginLink(context: context),
            ],
          ),
        ),
      ),
    );
  }

  BlocConsumer<RegisterCubit, RegisterState> _registerState(Size screenSize) {
    return BlocConsumer(
      listener: (context, state) {
        if (state is OtpVerified) {
          RegisterUser registerUser =
              RegisterUser.fromJson(_formKey.currentState!.value);

          //Get Device Platform
          String devicePlatform = '';
          if (Platform.isAndroid) {
            devicePlatform = 'Android';
          } else if (Platform.isIOS) {
            devicePlatform = 'iOS';
          }

          registerUser.platform = devicePlatform;
          registerUser.deviceToken = 'device token';

          //Get user avatar
          if (_image != null) {
            List<int> imageBytes = _image!.readAsBytesSync();
            String base64Image = base64Encode(imageBytes);
            registerUser.avatar = base64Image;
          }

          locator.get<RegisterCubit>().register(
              registerUser: registerUser, termsConditions: _termsConditions);
          // Submit from data
        }
      },
      builder: (context, state) {
        if (state is RegisterInitial) {
          return Container();
        } else if (state is RegisterLoading) {
          return Center(
            child: LoadingIndicator(),
          );
        } else if (state is RegisterCompleted) {
          return Text('Done');
        } else if (state is RegisterError) {
          print(state.message);
          var error = '';
          if (state.message.contains('422')) {
            error =
                locator.get<Helper>().getValidationError(string: state.message);
          } else if (state.message.contains('الشروط') ||
              state.message.contains('conditions')) {
            error = state.message;
          } else {
            error = LocaleKeys.something_went_wrong_please_try_again_later.tr();
          }
          return ValidationError(screenSize: screenSize, error: error);
        } else if (state is OtpCodeSending) {
          return LoadingIndicator();
        } else if (state is OtpCodeSent) {
          return Container();
        } else if (state is OtpError) {
          return ValidationError(
              screenSize: screenSize,
              error: LocaleKeys.failed_to_send_verification_code_try_again_later
                  .tr());
        }
        return Container();
      },
    );
    // return BlocBuilder<RegisterCubit, RegisterState>(
    //
    // );
  }

  Padding _submitButton({required Size screenSize}) {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0),
      child: Center(
        child: RoundedButton(
          title: LocaleKeys.login.tr(),
          onPressed: () {
            if (_formKey.currentState!.saveAndValidate()) {
              RegisterUser registerUser =
                  RegisterUser.fromJson(_formKey.currentState!.value);

              locator
                  .get<RegisterCubit>()
                  .sendOtp(phoneNumber: registerUser.phone!, context: context);
            }
          },
          color: AppTheme.primaryColor,
          radius: 40,
          height: 40,
          width: screenSize.width / 2.3,
          borderColor: AppTheme.primaryColor,
        ),
      ),
    );
  }

  _termsConditionsWidget({required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 12.0, right: 3, bottom: 12.0),
      child: Row(
        children: [
          RoundCheckBox(
            size: 20,
            onTap: (value) {
              setState(() {
                _termsConditions = value!;
              });
            },
            checkedColor: Colors.white,
            checkedWidget: Icon(
              Icons.circle,
              size: 15,
              color: AppTheme.primaryColor,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 10.0, left: 10.0),
            child: RichText(
              text: TextSpan(
                text: '${LocaleKeys.agree_with.tr()}  ',
                style: Theme.of(context).textTheme.subtitle2,
                children: <TextSpan>[
                  TextSpan(
                    text: LocaleKeys.terms_conditions.tr(),
                    style: Theme.of(context).textTheme.subtitle2!.copyWith(
                        color: AppTheme.primaryColor,
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print('aaa');
                      },
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _loginLink({required BuildContext context}) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0, bottom: 80.0),
        child: RichText(
          text: TextSpan(
              text: LocaleKeys.do_have_an_account.tr(),
              style: Theme.of(context).textTheme.subtitle2,
              children: <TextSpan>[
                TextSpan(
                    text: ' ${LocaleKeys.login.tr()}',
                    style: TextStyle(
                        color: AppTheme.primaryColor,
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login-page', (Route<dynamic> route) => false);
                      })
              ]),
        ),
      ),
    );
  }

  _fullName({required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: FormBuilderTextField(
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.symmetric(vertical: 7.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: AppTheme.primaryColor,
              radius: 3,
              child: Icon(
                PhosphorIcons.user,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
          hintStyle: Theme.of(context)
              .textTheme
              .subtitle2!
              .copyWith(color: Colors.grey),
          hintText: LocaleKeys.username.tr(),
        ),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(
            context,
            errorText: LocaleKeys.the_username_field_is_requires.tr(),
          ),
        ]),
        name: 'name',
      ),
    );
  }

  _fullPhoneNumber({required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: FormBuilderTextField(
        keyboardType: TextInputType.phone,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.symmetric(vertical: 7.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: AppTheme.primaryColor,
              radius: 3,
              child: Icon(
                PhosphorIcons.phone,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
          hintStyle: Theme.of(context)
              .textTheme
              .subtitle2!
              .copyWith(color: Colors.grey),
          hintText: LocaleKeys.phone.tr(),
        ),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(
            context,
            errorText: LocaleKeys.the_phone_field_is_requires.tr(),
          ),
          // (val) {
          //   Pattern? pattern = r'^\+?09[0-9]{9}$';
          //   RegExp regex = RegExp(pattern.toString());
          //   if (!regex.hasMatch(val!)) {
          //     return LocaleKeys.please_enter_valid_phone_number.tr();
          //   }
          //   // print();
          // }
        ]),
        name: 'phone',
      ),
    );
  }

  _fullPassword({required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: FormBuilderTextField(
        enableSuggestions: false,
        autocorrect: false,
        obscureText: true,
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.symmetric(vertical: 7.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: AppTheme.primaryColor,
              radius: 3,
              child: Icon(
                PhosphorIcons.lock,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
          hintStyle: Theme.of(context)
              .textTheme
              .subtitle2!
              .copyWith(color: Colors.grey),
          hintText: LocaleKeys.password.tr(),
        ),
        //
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(
            context,
            errorText: LocaleKeys.the_password_field_is_requires.tr(),
          ),
          FormBuilderValidators.minLength(
            context,
            8,
            errorText: LocaleKeys.the_password_is_more_than_8_characters.tr(),
          ),
          (val) {
            if (val !=
                _formKey.currentState!.fields['confirm_password']?.value) {
              return LocaleKeys.passwords_does_not_match.tr();
            }
          }
        ]),
        name: 'password',
      ),
    );
  }

  _fullConfirmPassword({required BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: FormBuilderTextField(
        enableSuggestions: false,
        autocorrect: false,
        obscureText: true,
        decoration: InputDecoration(
          filled: true,
          contentPadding: const EdgeInsets.symmetric(vertical: 7.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: AppTheme.primaryColor,
              radius: 3,
              child: Icon(
                PhosphorIcons.lock,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
          hintStyle: Theme.of(context)
              .textTheme
              .subtitle2!
              .copyWith(color: Colors.grey),
          hintText: LocaleKeys.confirm_password.tr(),
        ),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(
            context,
            errorText: LocaleKeys.the_password_field_is_requires.tr(),
          ),
          FormBuilderValidators.minLength(
            context,
            8,
            errorText: LocaleKeys.the_password_is_more_than_8_characters.tr(),
          ),
          (val) {
            if (val != _formKey.currentState!.fields['password']?.value) {
              return LocaleKeys.passwords_does_not_match.tr();
            }
          }
        ]),
        name: 'confirm_password',
      ),
    );
  }

  Padding _profileAvatarWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 30.0, bottom: 20),
      child: InkWell(
        onTap: getImage,
        child: PingAvatar(
          size: 100,
          image: _image == null
              ? Image.asset('assets/images/user.png',
                  fit: BoxFit.fill, height: 100, width: 100)
              : Image.file(
                  _image!,
                  fit: BoxFit.fill,
                  height: 100,
                  width: 100,
                ),
          bottomRight: Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppTheme.primaryColor,
              shape: BoxShape.circle,
            ),
            child: Icon(
              PhosphorIcons.camera_bold,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Column _titleWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          LocaleKeys.welcome_to,
          style: Theme.of(context)
              .textTheme
              .headline5!
              .copyWith(color: AppTheme.primaryColor),
        ).tr(),
        Text(
          LocaleKeys.ping,
          style: Theme.of(context).textTheme.headline3!.copyWith(
              color: AppTheme.primaryColor, fontFamily: 'tajawal-bold'),
        ).tr(),
        Text(
          LocaleKeys.we_provide_best_offers_of_i_s_p,
          style: Theme.of(context)
              .textTheme
              .subtitle2!
              .copyWith(color: AppTheme.grey.withOpacity(0.4)),
        ).tr(),
      ],
    );
  }
}
