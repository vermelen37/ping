import 'package:ping/data/model/authentication/register_user.dart';
import 'package:ping/data/model/auth_user.dart';
import 'package:ping/data/repository/authentication/authentication_repository.dart';
import 'package:ping/utils/api/api_base_helper.dart';

import '../../../locator.dart';

class AuthenticationRepositoryImpl extends AuthenticationRepository {
  @override
  Future<void> forgotPassword(String phone) {
    // TODO: implement forgotPassword
    throw UnimplementedError();
  }

  @override
  Future<AuthUser> getCurrentUser() {
    // TODO: implement getCurrentUser
    throw UnimplementedError();
  }

  @override
  Future<bool> isAuthenticated() {
    // TODO: implement isAuthenticated
    throw UnimplementedError();
  }

  @override
  Future<void> login({required String phone, required String password}) {
    // TODO: implement login
    throw UnimplementedError();
  }

  @override
  Future<void> logout() {
    // TODO: implement logout
    throw UnimplementedError();
  }

  @override
  Future<AuthUser> register({required RegisterUser registerUser}) async {
    var response =
        await locator.get<ApiBaseHelper>().postDio('register', registerUser);
    return AuthUser.fromJson(response);
  }
}
