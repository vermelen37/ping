import 'package:ping/data/model/authentication/register_user.dart';
import 'package:ping/data/model/auth_user.dart';

abstract class AuthenticationRepository {
  Future<AuthUser> register({required RegisterUser registerUser});

  Future<void> login({required String phone, required String password});

  Future<bool> isAuthenticated();

  Future<AuthUser> getCurrentUser();

  Future<void> forgotPassword(String phone);

  Future<void> logout();
}
