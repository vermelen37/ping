class RegisterValidation {
  int? status;
  Message? message;

  RegisterValidation({required this.status, required this.message});

  RegisterValidation.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message =
        json['message'] != null ? new Message.fromJson(json['message']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message!.toJson();
    }
    return data;
  }
}

class Message {
  List<String>? name;
  List<String>? phone;
  List<String>? password;

  Message({required this.name, required this.phone, required this.password});

  Message.fromJson(Map<String, dynamic> json) {
    name = json['name'].cast<String>();
    phone = json['phone'].cast<String>();
    password = json['password'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['password'] = this.password;
    return data;
  }
}
