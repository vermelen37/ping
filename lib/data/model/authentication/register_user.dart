class RegisterUser {
  String? name;
  String? phone;
  String? password;
  String? deviceToken;
  String? platform;
  String? avatar;

  RegisterUser(
      {this.name,
        this.phone,
        this.password,
        this.deviceToken,
        this.platform,
        this.avatar});

  RegisterUser.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phone = json['phone'];
    password = json['password'];
    deviceToken = json['device_token'];
    platform = json['platform'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['device_token'] = this.deviceToken;
    data['platform'] = this.platform;
    data['avatar'] = this.avatar;
    return data;
  }
}