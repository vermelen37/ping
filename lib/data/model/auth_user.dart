class AuthUser {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? platform;
  String? deviceToken;
  String? accessToken;
  String? avatar;

  AuthUser(
      {this.id,
        this.name,
        this.email,
        this.phone,
        this.platform,
        this.deviceToken,
        this.accessToken,
        this.avatar});

  AuthUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    platform = json['platform'];
    deviceToken = json['device_token'];
    accessToken = json['access_token'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['platform'] = this.platform;
    data['device_token'] = this.deviceToken;
    data['access_token'] = this.accessToken;
    data['avatar'] = this.avatar;
    return data;
  }
}