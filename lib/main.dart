import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:ping/router.dart';
import 'package:ping/utils/cache_helper.dart';
import 'package:ping/utils/theme/themes.dart';

import 'locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  Firebase.initializeApp();

  setupInjection();

  CacheHelper.init();

  await GlobalConfiguration()
      .loadFromPath('assets/configuration/app_settings.json');

  //check if logged in
  bool isLoggedIn = CacheHelper.getData(key: 'isLoggedIn') ?? false;
  String initialRoute;

  if (isLoggedIn) {
    initialRoute = '/';
  } else {
    initialRoute = '/register';
  }

  runApp(
    EasyLocalization(
      path: 'assets/translations',
      supportedLocales: [Locale('ar')],
      child: EasyDynamicThemeWidget(
        child: MyApp(
          router: AppRouter(),
          initialRoute: initialRoute,
        ),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  final AppRouter router;
  final String initialRoute;

  const MyApp({required this.router, required this.initialRoute}) : super();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: lightThemeData,
      darkTheme: darkThemeData,
      locale: context.locale,
      initialRoute: initialRoute,
      onGenerateRoute: router.generateRoute,
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      themeMode: EasyDynamicTheme.of(context).themeMode,
    );
  }
}
