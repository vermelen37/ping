import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ping/cubit/app/app_navigation_cubit.dart';
import 'package:ping/cubit/authentication/register_cubit.dart';
import 'package:ping/screens/authentication/otp_page.dart';
import 'package:ping/screens/authentication/register_page.dart';
import 'package:ping/screens/home_layout.dart';

import 'locator.dart';

class AppRouter {
  Route? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<AppNavigationCubit>(
                create: (BuildContext context) =>
                    locator.get<AppNavigationCubit>(),
              ),
            ],
            child: HomeLayout(),
          ),
          // builder: (BuildContext context) => HomeLayout(),
        );

      case '/register':
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<RegisterCubit>(
                create: (BuildContext context) => locator.get<RegisterCubit>(),
              ),
            ],
            child: RegisterPage(),
          ),
        );
      case '/otp':
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider<RegisterCubit>(
                create: (BuildContext context) => locator.get<RegisterCubit>(),
              ),
            ],
            child: OtpPage(),
          ),
        );
      // case ADD_NOTE_ROUTE:
      //   return MaterialPageRoute(
      //     builder: (_) => BlocProvider(
      //       lazy: true,
      //       create: (context) => AddNoteCubit(),
      //       child: AddNoteScreen(),
      //     ),
      //   );
      // case EDIT_NOTE_ROUTE:
      //   return MaterialPageRoute(builder: (_) => EditNoteScreen());
      // default:
      //   return null;
    }
  }
}
