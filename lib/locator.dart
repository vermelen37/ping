import 'package:get_it/get_it.dart';
import 'package:ping/data/repository/authentication/authentication_repository_impl.dart';
import 'package:ping/utils/api/api_base_helper.dart';
import 'package:ping/utils/helper.dart';
import 'cubit/app/app_navigation_cubit.dart';
import 'cubit/authentication/register_cubit.dart';

final locator = GetIt.instance;

void setupInjection() {
  setupCubits();
}

void setupCubits() {
  locator.registerLazySingleton<Helper>(() => Helper());
  locator.registerLazySingleton<AppNavigationCubit>(() => AppNavigationCubit());
  locator.registerLazySingleton<ApiBaseHelper>(() => ApiBaseHelper());
  locator.registerLazySingleton<RegisterCubit>(() => RegisterCubit());
  locator.registerLazySingleton<AuthenticationRepositoryImpl>(
      () => AuthenticationRepositoryImpl());
  // locator.registerLazySingleton<NotesRepository>(() => NotesRepository());
}